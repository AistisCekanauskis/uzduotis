<?php

$host = 'localhost';
$dbname = 'uzduotis';
$user = 'root';
$password = '';

try {
	$db = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
	echo $ex->getMessage();
}

include_once 'Statements/statements.php';
$statements = new Statements($db);
