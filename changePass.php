<?php

session_start();

include_once 'Functions/class.sidebar.php';

if(!isset($_SESSION["logged_in"])) {
	header('Location: login.php');
}
$errorMsg = null;

if(isset($_POST['submit'])) {
	$old_pass = htmlspecialchars($_POST['old_pass']);
	$new_pass1 = htmlspecialchars($_POST['new_pass1']);
	$new_pass2 = htmlspecialchars($_POST['new_pass2']);
	if(!empty($old_pass) && !empty($new_pass1) && !empty($new_pass2)) {
		$changePass = new changePass();
		$errorMsg = $changePass->changePass($_SESSION["user_id"], $old_pass, $new_pass1, $new_pass2);
	}
}
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<div class="row">
	<?php include_once 'Parts/sidebar.php'; ?>
		<div class="col-xs-12 col-md-7">
			<div class="main">
			<?php if($errorMsg == 0) { ?>
			<h3>Slaptažodžio keitimas:</h3>
				<form action=" <?php $_SERVER['PHP_SELF'] ?>" method="POST">
					<p> Įveskite seną slaptažodį <br><input type="text" name="old_pass"></p>
					<p> Įveskite naują slaptažodį <br><input type="text" name="new_pass1"></p>
					<p> Pakartokite naują slaptažodį <br><input type="text" name="new_pass2"></p>
					<p style="color:red;"><?php isset($errorMsg) ? print $errorMsg : ''; ?></p>
					<input type="submit" value="Pakeisti" name="submit">
				</form>
			<?php } else { ?>
				<h3>Slaptažodis sėkmingai pakeistas<h3>
				<div class="button">
					<a href="index.php">Grįžti į pagrindinį meniu</a>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</body>
</html>