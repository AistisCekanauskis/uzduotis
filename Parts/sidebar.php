<div class="col-xs-12 col-md-3" style="min-width: 400px;">
	<div class="sidebar">
		<div class="row">
			<div class="col-md-6 col-xs-6 col-sm-6">
				<p style="text-align:center; margin-top: 6px;">Sveiki <b><?php echo $_SESSION["logged_in"]?></b>.</p>
			</div>

			<div class="col-md-6 col-xs-6 col-sm-6">
				<div class="button">
					<a href="log_out.php">Atsijungti nuo paskyros</a>
				</div>
			</div>
		</div>

		<div class="sidebar_info">
			<p class="buttonas"><a href="changePass.php">Pakeisti slaptažodį</a></p>
		</div>
		<div class="sidebar_info">
			<p class="buttonas"><a href="index.php">Sugrįžti į pagrindinį puslapį</a></p>
		</div>
		<div class="sidebar_info">
			<p class="buttonas"><a href="profile.php">Redaguoti profilį</a></p>
		</div>

		<?php 

			require_once './Functions/class.admin.php';
			$user = new Admin();
			$check = $user->isAdmin();

			if($check) {
		?>
		<div class="sidebar_info">
			<p class="buttonas"><a style="color:red" href="admin.php">Admin Control Panel</a></p>
		</div>

		<?php } ?>

	</div>
</div>