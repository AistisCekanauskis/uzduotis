<?php

session_start();

include_once 'Functions/class.profilis.php';

if(isset($_POST['update'])) {

	$username = htmlspecialchars($_POST['username']);
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$f_name = htmlspecialchars($_POST['first_name']);
	$l_name = htmlspecialchars($_POST['last_name']);
	$user_id = (int)$_POST['user_id'];

	if(!empty($username) && !empty($email) && !empty($f_name) && !empty($l_name) && !empty($user_id)) {

		$user_info_upd = new Profile();
		$Msg = $user_info_upd->edit_profile_info($username, $email, $f_name, $l_name, $user_id);

	}


}
?>

<?php include_once 'Parts/header.php' ?>

<body>
	
	<div class="row">
	<?php include_once 'Parts/sidebar.php'; ?>

		<div class="col-xs-12 col-md-7">
			<div class="main">

				<div class="comment">
					<h3>Mano profilis:</h3>	
					<p>Profilio nuotrauka:</p>
					<?php 
						$prof_img = new Profile();
						echo $prof_img->profile_photo();

					?>
				</div><br>

				<div class="comment">
					<h3>Visa bendra informacija</h3>
					<?php
						isset($Msg) ? print $Msg : false;
						$profile_info = new Profile();
						echo $profile_info->show_profile_info();
					?>
				</div>
			</div>
		<div class="col-xs-12 col-md-8">
	</div>

</body>

</html>