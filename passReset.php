<?php

session_start();

include_once('Functions/user.php');

if(isset($_POST['button'])) {
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	
	if(isset($username) && isset($password) ) {
		$user = new User($username, $password);
		$user->login();
	}
}
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<form action="login.php" method="POST">
		<p>Naujas slaptažodis<br>
		<input type="password" name="password"></p>
		<p>Pakartoti slaptažodį<br>
		<input type="password" name="password2"></p>
		<input type="submit" value="Prisijungti" name="button">
	</form>
	<a href="login.php">Grįšti į prisijungimo langą</a><br>
</body>
</html>