<?php

session_start();

include_once('Functions/user.php');

$success = 0;

if(isset($_POST['button'])) {
	$email = htmlspecialchars($_POST['email']);
	
	if(isset($email) && !empty($email)) {
		$emailReset = new User_pass_reset();
		$emailReset->passResetEmail($email);
		$success = $emailReset->getSuccess();
	}
}
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<?php if ($success == 0) { ?>
	<form action="passResetEmail.php" method="POST">
		<p>Įveskite El. paštą susietą su jūsų paskyra<br>
		<input type="email" name="email"></p>
		<input type="submit" value="Prisijungti" name="button">
	</form>
	<a href="login.php">Grįšti į prisijungimo langą</a><br>
	<?php } else { ?>
	<p>Jūsų naujas slaptažodis sėkmingai išsiųstas į jūsų El. paštą.</p>
	<?php } ?>
</body>
</html>