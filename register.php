<?php

session_start();

include_once('Functions/user.php');

if(isset($_POST['button'])) {
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	$password2 = htmlspecialchars($_POST['password2']);
	$email = htmlspecialchars($_POST['email']);

	if(!empty($username) && !empty($password) && !empty($password2) && !empty($email)) {
		$user = new User_Register($username, $password, $password2, $email);
		$errorMsg = $user->register();
	}
}
$msg = '<p style="color:red">Vartotojas su tokiu vardu jau egzistuoja, bandykite dar kartą.</p>';
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<div class="register_form">
	<h3>Registracija</h3>
	<form action="register.php" method="POST">
		<p>Vardas<br>
		<input type="text" name="username" value="<?php isset($username) ? print $username : '' ?>"></p>
		<p>Slaptažodis<br>
		<input type="password" name="password"></p>
		<p>Pakartoti slaptažodis<br>
		<input type="password" name="password2"></p>
		<p>El. Paštas<br>
		<input type="email" name="email" value="<?php isset($email) ? print $email : '' ?>"></p>
		<?php isset($errorMsg) ? print $errorMsg : ''; ?>
		<input type="submit" value="Prisiregistruoti" name="button">
	</form>
	<a href="login.php">Jau esate prisiregistravę?</a>
	</div>
</body>
</html>