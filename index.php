<?php

session_start();

include_once('Functions/comments.php');

/*-----  Check if logged in ----*/
if(!isset($_SESSION["logged_in"])) {
	header('Location: login.php');
}
/*-----  Check if edit post button clicked -----*/
isset($_POST['update']) ? $update = 1 : $update = 0;

/*-----  check if confirm_edit_post button is clicked -----*/
if(isset($_POST['upd_button'])) 
{
	$subject = htmlspecialchars($_POST['subject']);
	$comment = htmlspecialchars($_POST['comment']);
	$commentId = htmlspecialchars($_POST['update_rec_id']);
	if(isset($subject) && !empty($subject) && isset($comment) && !empty($comment)) 
	{
		$updComment = new Comments();
		$errorMsg = $updComment->updComment($subject, $comment, $commentId);
	}
}

/*-----  Check if comment post button is clicked -------*/
if(isset($_POST['button'])) 
{
	$subject = htmlspecialchars($_POST['subject']);
	$comment = htmlspecialchars($_POST['comment']);
	if(isset($subject) && !empty($subject) && isset($comment) && !empty($comment))
	{
		$comments = new Comments();
		$comments->postComment($subject, $comment, $_SESSION['user_id']);
		$errorMsg = $comments->getErrorMsg();
	}
}
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<div class="row">
	<?php include_once 'Parts/sidebar.php'; ?>
		<div class="col-xs-12 col-md-7">
		<div class="main">
			<div class="comment">
				<h3>Rašykite komentarą:</h3>

				<?php include_once 'Functions/forms.php'; 
					if(isset($errorMsg))
						echo $errorMsg;

					$Forms = new Forms();
						if($update == 0) { 
							echo $Forms->form();
						} else { 
					 		echo $Forms->updForm($_POST['update_rec_id']);
					  } ?>

				<hr>
				<div class="comment_post">
						<?php 
						$comments = new Comments(); 
						$comments->getComment($_SESSION['user_id'])

						?>
				</div><br>
			</div>
		</div>
		<div class="col-xs-12 col-md-8">
		</div>
		</div>
	</div>
</body>
</html>