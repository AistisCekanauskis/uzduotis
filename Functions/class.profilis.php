<?php

class Profile {

	public function __construct() {

	}

	public function profile_photo() {

		require './db.php';

		$user_id = (int)$_SESSION['user_id'];

		if(isset($_POST['prof_img_button'])) {

			$tmpfileName = $_FILES['file_name']['tmp_name'];
			$fileName = $_FILES['file_name']['name'];

			move_uploaded_file($tmpfileName, "uploaded_files/". $fileName);

			$upd_img = $db->prepare("UPDATE users SET prof_image = ? WHERE id = ?");
			$upd_img->execute(array($fileName, $user_id));
		}

		$query = $db->prepare("SELECT id, prof_image FROM users WHERE id = ?");
		$query->execute(array($user_id));
		$prof = $query->fetch(PDO::FETCH_OBJ);

		/*------------ file upload buttons -------------*/
		$msg = '<form action="" method="post" enctype="multipart/form-data">';
		$msg .= '<input type="hidden" name="MAX_FILE_SIZE" value="2000000">';
		$msg .= '<input type="file" name="file_name"><br>';
		$msg .= '<input type="submit" name="prof_img_button" value="Įkelti"></form>';

		if($prof->prof_image == "") {

			echo "<img width='100' height='100' src='./images/profile_placeholder.gif' alt='Default profile image' />";
		
		} else {

			echo "<img width='100' height='100' src='./images/$prof->prof_image' alt='Default profile image' />";
			
		}

		return $msg;
	}


	public function show_profile_info() {

		require './db.php';

		$user_id = (int)$_SESSION['user_id'];


		$query = $db->prepare("SELECT * FROM users WHERE id = ?");
		$query->execute(array($user_id));
		$user_info = $query->fetch(PDO::FETCH_OBJ);

		$msg  = '<form id="update" method="post" action="">';
		$msg .= '<input type="hidden" name="user_id" value="' . $user_id . '">';
		$msg .= '<p>Vardas <br> <input type="text" name="username" value="'.$user_info->username.'"></p>';
		$msg .= '<p>Emailas <br> <input type="email" name="email" value="'.$user_info->e_mail.'"></p>';
		$msg .= '<p>Vardas <br> <input type="text" name="first_name" value="'.$user_info->first_name.'"></p>';
		$msg .= '<p>Pavarde <br> <input type="text" name="last_name" value="'.$user_info->last_name.'"></p>';
		$msg .= '<input type="submit" name="update" value="Atnaujinti">';
		$msg .= '</form>';

		return $msg;
	}

	public function edit_profile_info($username, $email, $f_name, $l_name, $user_id) {

		require './db.php';

		$query = $db->prepare("
			UPDATE users 
			SET username   = ? , 
				first_name = ? , 
				last_name  = ? ,
				e_mail     = ?
			WHERE id = $user_id ");
		$query->bindParam( 1 , $username, PDO::PARAM_STR);
		$query->bindParam( 2 , $f_name, PDO::PARAM_STR);
		$query->bindParam( 3 , $l_name, PDO::PARAM_STR);
		$query->bindParam( 4 , $email);
		$query->execute();

		return '<p style="color:green">Jūsų duomenys sėkmingai atnaujinti.</p>';

	}
}