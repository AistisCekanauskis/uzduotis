<?php

class Admin {

	private $user_id;

	public function __construct() {

	}

	public function isAdmin() {

		require 'db.php';

		$this->user_id = $_SESSION["user_id"];

		$query = $db->prepare("SELECT type FROM users WHERE id = ?");
		$query->bindparam( 1 , $this->user_id , PDO::PARAM_INT);
		$query->execute();
		$is_admin = $query->fetch(PDO::FETCH_OBJ);

		if($is_admin->type == 1) {

			return true;

		} else {

			return false;

		}
	}
}

class Admin_powers extends Admin {

	public function __construct() {

	}

	public function showUsers() {

		require './db.php';

		$query = $db->prepare("SELECT * from users");
		$query->execute();
		$users = $query->fetchAll();

		$msg  = '<table class="table table-striped"><thead><tr>';
		$msg .= '<th>User ID</th><th>Username</th><th>First Name</th><th>Last Name</th><th>Password</th><th>Email</th><th>Join Date</th><th>Type</th><th>GodMode</th>';
		$msg .= '</tr></thead><tbody>';

		foreach($users as $user) {

			$user['type'] == 1 ? $type = 'ADMIN' : $type = 'USER';
			$msg .= '<tr><td>' . $user['id']         . '</td>';
			$msg .= 	'<td>' . $user['username']   . '</td>';
			$msg .= 	'<td>' . $user['first_name'] . '</td>';
			$msg .= 	'<td>' . $user['last_name']  . '</td>';
			$msg .= 	'<td>' . $user['password']   . '</td>';
			$msg .= 	'<td>' . $user['e_mail']     . '</td>';
			$msg .= 	'<td>' . $user['join_date']  . '</td>';
			$msg .= 	'<td>' . $type               . '</td>';
			$msg .=		'<td><a href="admin.php?edit=' . $user['id'] . '">Edit</a></td></tr>';
		}  

		$msg .= '</tbody></table>';

		return $msg;
	}

	public function editUser($user_id) {

		require './db.php';

		$query = $db->prepare('SELECT * FROM users WHERE id = ?');
		$query->bindparam( 1 , $user_id , PDO::PARAM_INT);
		$query->execute();
		$user = $query->fetch(PDO::FETCH_OBJ);

		include_once 'forms.php';
		$form = new Forms();
		echo $form->user_upd_form($user_id, $user->e_mail);
	}

	public function updUser() {


	}
}