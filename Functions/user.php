<?php

class User {
	protected $username;
	protected $password;
	private $email;

	public function __construct($username, $password) {
		$this->username = $username;
		$this->password = md5($password);
	}

	public function login() {
		try {
			include './db.php';
			$statements->loginStat($this->username, $this->password);
			foreach($statements->getStat() as $stat) {
				if($this->username == $stat['username'] && $this->password == $stat['password']) {
					$_SESSION["logged_in"] = $stat["username"];
					$_SESSION["user_id"] = $stat["id"];
					header('Location: index.php');
				}
			}

			/*-- If no users found with this username and password --*/
			$statement = null;
			return '<p style="color:red;">Blogai suvesti duomenys, bandykite dar kartą.</p><br>';
			
		} catch (PDOException $ex) {
			$ex->getMessage();
		}
	}
}

class User_Register extends User {
	private $password2;

	public function __construct($username, $password, $password2, $email) {
		parent::__construct($username, $password);
		$this->password2 = md5($password2);
		$this->email = $email;
	}

	public function register() {
		include './db.php' ;
		$statements->ifNameExists($this->username);
		$rowCount = $statements->getRowCount();
		if($rowCount == 0)
		{
			if($this->password == $this->password2) 
			{
				$statements->registerStat($this->username, $this->password, $this->email);
				header('Location: login.php');
			} else {
				return '<p style="color:red">Slaptažodžiai nesutampa, bandykite dar kartą</p><br>';
			}
		} else {
			return '<p style="color:red">Vartotojas su tokiu vardu jau egzistuoja, bandykite dar kartą.</p>';
		}
	}
}
/*class User_pass_reset extends User_Register 
{
	private $success;

	public function __construct() 
	{}

	public function getSuccess()
	{
		return $this->success;
	}

	public function passResetEmail($email)
	{
		include 'db.php' ;
		$statements->ifEmailExists($email);
		$rowCount = $statements->getRowCount();
		if($rowCount >= 1)
		{	
			require_once "phpmailer/manoForma.php";
		} else {
			echo "Paskyra su šiuo elektroniniu paštu neegizstuoja.";
			$this->success = 0;
		}
	}
}*/