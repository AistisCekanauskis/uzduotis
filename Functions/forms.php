<?php 

class Forms 
{
	public function __construct() {}

	public function form() {
		$msg = '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST">';
		$msg .= '<p>Subject:<br><input type="text" name="subject" size="18"></p>';
		$msg .= '<p>Comment box:<br><textarea name="comment" rows="4" cols="50" ></textarea></p>';
		$msg .= ' <input type="submit" name="button" value="Rašyti">';
		$msg .= '</form>';

		return $msg;
	}

	public function updForm($post_id) {
		include './db.php';
		$statements->getUpdCommentForm($post_id);
		foreach( $statements->getStat() as $stat ) 
		{
			$msg = '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST">';
			$msg .= '<p>Subject:<br><input type="text" value ="' . $stat['subject'] . '" name="subject" size="18"></p>';
			$msg .= '<input type="hidden" name="update_rec_id" value="' . $post_id;
			$msg .= '"><p>Comment box:<br><textarea name="comment" rows="4" cols="50" >' . $stat['comment'] . '</textarea></p>';
			$msg .= ' <input type="submit" name="upd_button" value="Atnaujinti">';
			$msg .= '</form>';
		}

		return $msg;
	}

	public function user_upd_form($user_id, $e_mail) {

		$msg = '<form action="' . $_SERVER['PHP_SELF'] . '" method="POST">';
		$msg .= '<input type="hidden" name="user_id" value="' . $user_id . '">';
		$msg .= '<p>New password:<br>';
		$msg .= '<input type="text" name="new_pass" size="18" ></p>';
		$msg .= '<p>Email:<br>';
		$msg .= '<input type="text" value="' . $e_mail . '" name="email" size="18" ></p>';
		$msg .= '<p>Type:<br>';
		$msg .= '<select name="type">';
		$msg .= '<option value="0">User</option> <option value="1">Admin</option></select><br><br>';
		$msg .= ' <input type="submit" name="edit_user" value="Atnaujinti">';
		$msg .= '</form>';

		return $msg;
	}	
}