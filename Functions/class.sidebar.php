<?php

class sidebar
{

	private $success = 0;

	public function __construct()
	{

	}

	public function getSuccess() 
	{
		return $this->success;
	}
}
class ChangePass extends sidebar
{
	public function __construct()
	{}
	public function changePass($userId, $old_pass, $new_pass1, $new_pass2)
	{
		include './db.php';
		$statements->oldPasswordValidation($userId, $old_pass);
		if($statements->getRowCount() > 0)
		{
			if($new_pass1 == $new_pass2)
			{
				$statements->changePassword($userId, $new_pass1);
				return $this->success = 1;
			} else {
				return "Nauji slaptažodžiai nėra vienodi, prašome pakartoti.";
			}
		} else {
			return "Blogai įvestas senas slaptažodis.";
		}

	}
}