<?php

class Comments 
{
	private $error_msg;

	public function __construct() 
	{
	}

	public function getErrorMsg() {

		return $this->error_msg;
	}

	public function postComment($subject, $comment, $userId)
	{
		include './db.php';
		$statements->insertComment($subject, $comment, $userId);
		$this->error_msg = '<p style="color:green">Jūsų komentaras sėkmingai pridėtas</p>';
	}


	public function getComment($userId)
	{
		include './db.php';

		if(isset($_POST['delete'])) {
			$statements->delComment($_POST['delete_rec_id']);
			echo '<p style="color:red">Jūsų komentaras sėkmingai ištrintas</p>';
		}

				/*----- IF update -----*/
		$_per_page = 5;
		$_cur_page = isset($_GET['page']) ? $_GET['page'] : 0;
		$_next_page = $_cur_page + 1;
		$_prev_page = $_cur_page - 1;
		$_start_from = $_per_page * ($_cur_page);
		$_count_till = $_start_from + $_per_page;

		$statements->getComment($userId);
		$rowCount = $statements->getRowCount();

		$_total_pages = ceil($rowCount / $_per_page);

		$msg = '<u>Iš viso komentarų:</u> <b>' . $rowCount . '</b>.<br><br>';

		/*----- IF delete ------*/
	

		$statements->get_Comment_With_Limit($_start_from, $_per_page);
		foreach($statements->getStat() as $stat) {
			if($stat['prof_image'] == "") {

				$stat['prof_image'] = "profile_placeholder.gif";

			}

			$msg .= $this->getCommentRating($stat['id'], $userId);

			/*---------- Sends message if user rating same comment second time within 24 hours ------------*/
			if(isset($_SESSION['comment_id']) && $stat['id'] == $_SESSION['comment_id']) {
				if(isset($_SESSION['er_msg_preventer']) && $_SESSION['er_msg_preventer'] == 0)
					if(isset($_SESSION['time_till_next_post'])) {

						$_SESSION['er_msg_preventer'] = 1;
						$msg .= $_SESSION['time_till_next_post'];
						
					}
			}
			/*------------------ END message -------------------------------------------------------------*/

			$msg .= '<div style="margin-top:5px;"><img width="100" height="100" src="./images/' . $stat['prof_image'] . '" alt="Default profile image" /></div>';
			$msg .= '<p>'. $stat['username'] . ' <br>[ ' . $stat['date_added'] . ' ] <br>';
			$msg .= '<b>Apie ką : </b>' . $stat['subject'] . '<br>';
			$msg .= $stat['comment'] . '</p>';

			if($_SESSION['user_id'] == $stat['user_id']) {

				$msg .= $this->delCommentBTN($stat['id']);
				$msg .= $this->updCommentBTN($stat['id']);

			}

			$msg .= '<hr>';

		}

		/*----- Button NEXT comment page------*/
		if($_start_from >= $_per_page) {
			if($_start_from == $_per_page) {

				$msg .= '<div class="button">
						 	<a href="index.php">Atgal</a>
						 </div>';

			} else {

				$msg .= '<div class="button">
							<a href="index.php?page=' . $_prev_page . '">Atgal</a>
						</div>';

			}
		}

		/*-------- Button PREVIOUS comment page-----*/
		if($rowCount > $_count_till) {
			$msg .= '<div class="post_button_holder">
					 	<div class="button">
					 		<a href="index.php?page=' . $_next_page . '">Sekantis</a>
					 	</div>
					 </div>'
					 ;
		}
		echo $msg;
		/*for($m = 0; $m < $_total_pages; $m++) {
			$n = $m + 1;
			echo '<a href="index.php?page=' . $m . '">' . $n . '</a>';
		}*/
		$statements = null;
	}


	/*-------- Update comment ----------*/
	public function updComment($subject, $comment, $commentId)
	{
		include './db.php';
		$statements->updComment($subject, $comment, $commentId);
		return '<p style="color:green">Jūsų komentaras sėkmingai antnaujintas.</p>';
	}


	/*----------- Delete button ------------*/
	public function delCommentBTN($stat) 
	{
		$msg = '<div class="buttons_comment">';
		$msg .= '<form id="delete" method="post" action="">';
		$msg .= '<input type="hidden" name="delete_rec_id" value="' . $stat;
		$msg .= '"><input type="submit" name="delete" value="Ištrinti komentarą!">';
		$msg .= '</form>';

		return $msg;
	}


	/*----------- Update button ------------*/
	public function updCommentBTN($stat)
	{
		$msg = '<form id="update" method="post" action="">';
		$msg .= '<input type="hidden" name="update_rec_id" value="' . $stat;
		$msg .= '"><input type="submit" name="update" value="Pakeisti komentarą!">';
		$msg .= '</form>';
		$msg .= '</div>';

		return $msg;
	}


	public function getCommentRating($comment_id, $userId)
	{
		include './db.php';
		$msg = '';

		foreach(range(1, 5) as $ratings) {
			$msg .= '<span class="button" style="margin-right: 3px;"><a href="rate.php?comment_id=' . $comment_id . '&rating=' . $ratings . '">'.  $ratings . '</a></span>';

		}

		$query = $db->prepare("
			SELECT 
				AVG(comment_rating.rate) AS rate
			FROM 
				Comments LEFT JOIN comment_rating 
			ON 
				comments.id = comment_rating.comment_id
			WHERE 
				comments.id = $comment_id
			");
		
		$query->execute();
		$rating = $query->fetch(PDO::FETCH_OBJ);

		$msg .= 'Rating: ' . round($rating->rate, PHP_ROUND_HALF_UP) . '/5';

		return $msg;
	}
}