<?php

class Statements 
{
	private $db;
	private $statement;
	private $rowCount;

	public function __construct($db) 
	{
		$this->db = $db;
	}

/*------------- ALL lagin/Register/Reset statements -------------*/
	public function getStat() 
	{
		return $this->statement;
	}

	public function getRowCount()
	{
		return $this->rowCount;
	}

	public function loginStat($username, $password) 
	{
		try {
			$sql = "
				SELECT 
					* 
				FROM 
					users 
				WHERE 
					username = ? and 
					password = ?
			";
			$statement = $this->db->prepare($sql);
			$statement->execute(array($username, $password));
			$this->statement = $statement;
		} catch (PDOException $ex) {
			echo "Klaida rasta naudojant šį statement : $sql <br>";
			echo $ex->getMessage();
		}
	}

	public function registerStat($username, $password, $email) 
	{
		try 
		{
			$datetime = date("Y-m-d H:i:s");
			$sql = "
				INSERT INTO 
					users (
					username,
					password,
					e_mail,
					join_date)
				VALUES 
					(?,?,?,?)
			";
			$statement = $this->db->prepare($sql);
			$statement->execute(array($username, $password, $email, $datetime));
		} 
		catch (PDOException $ex) 
		{
			echo "Klaida rasta naudojant šį statement : $sql <br>";
			echo $ex->getMessage();
		}
	}

	public function ifNameExists($username) 
	{
		try
		{
			$sql = "
				SELECT 
					* 
				FROM 
					users 
				WHERE 
					username = ?
			";
			$statement = $this->db->prepare($sql);
			$statement->execute(array($username));
			
			$this->rowCount = $statement->rowCount();
		}
		catch (PDOException $ex)
		{
			echo "Klaida rasta naudojant šį statement : $sql <br>";
			echo $ex->getMessage();
		}
	}

	public function ifEmailExists($email)
	{
		try
		{
			$sql = 
				"SELECT 
					* 
				FROM 
					users 
				WHERE 
					e_mail = ?
			";
			$statement = $this->db->prepare($sql);
			$statement->execute(array($email));

			$this->rowCount = $statement->rowCount();
		}
		catch (PDOException $ex)
		{
			echo "Klaida rasta naudojant šį statement : $sql <br>";
			echo $ex->getMessage();
		}
	}

	/*--------------- Comments section -------------*/
	public function insertComment($subject, $comment, $userId)
	{
		try
		{
			$datetime = date("Y-m-d H:i:s");
			$sql = 
				"INSERT INTO 
					comments (
					user_id,
					subject,
					comment,
					date_added)
				VALUES 
					(?,?,?,?)
			";
			$statement = $this->db->prepare($sql);
			$statement->execute(array($userId, $subject, $comment, $datetime));
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}

	public function getComment($comment_id)
	{
		try
		{
			$sql = 
				"SELECT 
					users.username, 
					comments.id, 
					comments.user_id,
					comments.subject, 
					comments.comment,
					comments.date_added
				FROM 
					users, comments 
				WHERE 
					users.id = comments.user_id
			";
			/*and user_id = ?*/
			/*$statement->bindparam(1, $comment_id);*/

			$statement = $this->db->prepare($sql);
			$statement->execute();
			$this->rowCount = $statement->rowCount();
			$this->statement = $statement;
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}

	public function get_Comment_With_Limit($start, $ammount)
	{
		try
		{
			$sql = 
				"SELECT 
					users.username,
					users.prof_image, 
					comments.id, 
					comments.user_id,
					comments.subject, 
					comments.comment,
					comments.date_added
				FROM 
					users, comments 
				WHERE 
					users.id = comments.user_id
				ORDER BY 
					comments.id DESC
				LIMIT 
					?, ?
			";
			
			$statement = $this->db->prepare($sql);
			$statement->bindparam(1, $start, PDO::PARAM_INT);
			$statement->bindValue(2, $ammount, PDO::PARAM_INT);
			$statement->execute();
			$this->statement = $statement;
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}


	public function delComment($id)
	{
		try
		{
			$sql = 
				"DELETE FROM 
					comments
				WHERE 
					id = ?"
			;
			$statement = $this->db->prepare($sql);
			$statement->execute(array($id));
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}

	public function updComment($subject, $comment, $commentId)
	{
		try
		{
			$sql = 
				"UPDATE 
					comments
				SET 
					subject = ?,
					comment = ?
				WHERE 
					id = ?"
			;
			$statement = $this->db->prepare($sql);
			$statement->execute(array($subject, $comment, $commentId));
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}

	public function getUpdCommentForm($comment_id)
	{
		try
		{
			$sql = 
				"SELECT 
					date_added,
					subject, 
					comment 
				FROM 
					comments 
				WHERE 
					id = ?"
			;
			$statement = $this->db->prepare($sql);
			$statement->execute(array($comment_id));
			$this->statement = $statement;
		}
		catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}


	public function ratingComment($comment_id)
	{
		try
		{
			$statement = $this->db->prepare("
				");
			$statement->execute(array($comment_id));
			return $statement->fetchAll();
		}
		catch (PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}


	public function changePassword($userId, $password)
	{
		try
		{
			$sql = 
				"UPDATE 
					users
				SET 
					password = ?
				WHERE 
					id = ?"
			;
			$statement = $this->db->prepare($sql);
			$statement->execute(array(md5($password), $userId));

		} catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}

	public function oldPasswordValidation($userId, $password)
	{
		try
		{
			$sql = 
				"SELECT
					*
				FROM
					users
				WHERE 
					password = ? and
					id = ?"
			;
			$statement = $this->db->prepare($sql);
			$statement->execute(array(md5($password),$userId));
			$this->rowCount = $statement->rowCount();

		} catch (PDOException $ex)
		{
			echo 'Klaida rasta naudojant šį statement : ' . $sql . '<br>';
			echo $ex->getMessage();
		}
	}
}