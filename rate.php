<?php 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


include_once 'db.php';

if(isset($_GET['comment_id'], $_GET['rating'])) {

	$comment_id = (int)$_GET['comment_id'];
	$rating = (int)$_GET['rating'];
	$user_id = $_SESSION['user_id'];
	
	if(in_array($rating, [1, 2, 3, 4, 5])) {

		$exists = $db->prepare("SELECT id FROM comments WHERE id = $comment_id");

		if($exists) {

			/*$limit = $db->prepare("
				SELECT * FROM comments 
				LEFT JOIN comment_rating 
				ON comments.user_id = comment_rating.user_id
				WHERE comment_rating.user_id = ? and comment_rating.comment_id = ?"
			);
			$limit->execute(array($user_id, $comment_id));*/

			$current_time_db = date("Y-m-d H:i:s");
			$current_time = strtotime($current_time_db);

			$query = $db->prepare("SELECT MAX(date_added) AS rate_date FROM comment_rating where comment_id = ? and user_id = ?");
			$query->execute(array($comment_id, $user_id));
			$rating_time = $query->fetch(PDO::FETCH_OBJ);
			$rating_time = strtotime($rating_time->rate_date) + 24*60*60;

			if($current_time > $rating_time) {

				$db->prepare("INSERT INTO comment_rating (comment_id, rate, user_id, date_added) VALUES ($comment_id, $rating, $user_id, ?)")->execute(array($current_time_db));
				if(isset($_SESSION['time_till_next_post'])) {
					unset($_SESSION['time_till_next_post']);
				} 
				if(isset($_SESSION['comment_id'])) {
					unset($_SESSION['comment_id']);
				}

			} else {

				$wait_time = $rating_time - $current_time;
				$hours = floor($wait_time / (60*60));
				$minute = floor(($wait_time - $hours*(60*60)) / 60);
				$seconds = floor($wait_time - $hours*(60*60) - $minute*60);
				$_SESSION['comment_id'] = $comment_id;
				$_SESSION['er_msg_preventer'] = 0;
				$_SESSION['time_till_next_post'] = '<p>Prašome palaukti <b style="color:red">'. $hours . 'h ' . $minute . 'min ' . $seconds . 'sec </b> iki kol galėsite vėl vertinti šį komentarą.</p>';

			}
		}
	}

	header("Location: index.php");
}
?>