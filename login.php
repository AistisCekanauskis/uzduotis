<?php

session_start();

include_once('Functions/user.php');

if(isset($_SESSION["logged_in"])) {
	header('Location: index.php');
}

if(isset($_POST['button'])) {
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	
	if(isset($username) && isset($password) ) {
		$user = new User($username, $password);
		$errorMsg = $user->login();
	}
}
?>

<?php include_once 'Parts/header.php' ?>
<body>
	<div class="login_form">
	<h3>PRISIJUNGIMAS</h3>
	<form action="login.php" method="POST">
		<p>Vardas<br>
		<input type="text" name="username" value="<?php isset($username) ? print $username : ''?>"></p>
		<p>Slaptažodis<br>
		<input type="password" name="password"></p>
		<?php (isset($errorMsg)) ? print $errorMsg : '' ?>
		<input type="submit" value="Prisijungti" name="button">
	</form>
	<a href="register.php">Registruotis</a><br>
	<!--<a href="passResetEmail.php">Pamiršote slaptažodį?</a>-->
	</div>
</body>
</html>